import React, { useContext, useEffect } from "react";
import {
	View,
	StyleSheet,
	Text,
	FlatList,
	TouchableOpacity
} from "react-native";
import { Feather } from "@expo/vector-icons";

import { Context } from "../context/BlogContext";

const IndexScreen = ({ navigation }) => {
	const { state, deleteBlogPost, getBlogPosts } = useContext(Context);
	useEffect(() => {
		getBlogPosts();

		const listener = navigation.addListener("didFocus", () => {
			getBlogPosts();
		});

		// called on unmount:
		return () => {
			listener.remove();
		};
	}, []);

	return (
		<View>
			<FlatList
				data={state}
				keyExtractor={blogPost => blogPost.id.toString()}
				renderItem={({ item }) => {
					return (
						<TouchableOpacity
							onPress={e => navigation.navigate("Show", { id: item.id })}
						>
							<View style={styles.row}>
								<Text style={styles.title}>{item.title}</Text>
								<TouchableOpacity onPress={() => deleteBlogPost(item.id)}>
									<Feather name="trash" style={styles.iconStyle} />
								</TouchableOpacity>
							</View>
						</TouchableOpacity>
					);
				}}
			/>
		</View>
	);
};

IndexScreen.navigationOptions = ({ navigation }) => {
	return {
		headerRight: () => (
			<TouchableOpacity
				onPress={() => {
					navigation.navigate("Create");
				}}
			>
				<Feather name="plus" size={30} />
			</TouchableOpacity>
		)
	};
};

const styles = StyleSheet.create({
	title: {
		fontWeight: "bold",
		fontSize: 18
	},
	row: {
		flexDirection: "row",
		justifyContent: "space-between",
		paddingVertical: 20,
		paddingHorizontal: 10,
		borderTopWidth: 1,
		borderColor: "grey"
	},
	iconStyle: {
		fontSize: 24
	}
});

export default IndexScreen;
