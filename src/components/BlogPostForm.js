import React, { useState, useContext } from "react";
import { View, Text, StyleSheet, TextInput, Button } from "react-native";
import { Context } from "../context/BlogContext";

const BlogPostForm = ({ onSubmit, initialFormValues }) => {
	const [title, setTitle] = useState(initialFormValues.title);
	const [content, setContent] = useState(initialFormValues.content);
	return (
		<View>
			<Text style={styles.label}>Enter title</Text>
			<TextInput
				value={title}
				onChangeText={text => setTitle(text)}
				style={styles.input}
			/>
			<Text style={styles.label}>Enter content</Text>
			<TextInput
				value={content}
				onChangeText={text => setContent(text)}
				style={styles.input}
			/>
			<Button title="Save blog post" onPress={e => onSubmit(title, content)} />
		</View>
	);
};

BlogPostForm.defaultProps = {
	initialFormValues: {
		title: "",
		content: ""
	}
};

const styles = StyleSheet.create({
	input: {
		fontSize: 18,
		borderWidth: 1,
		borderColor: "black",
		marginBottom: 15,
		padding: 5,
		margin: 5
	},
	label: {
		fontSize: 20,
		marginBottom: 5,
		marginLeft: 5
	}
});

export default BlogPostForm;
