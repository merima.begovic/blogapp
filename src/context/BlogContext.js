import createDataContext from "./createDataContext";
import jsonServer from "../api/jsonServer";
import Axios from "axios";

const blogReducer = (state, action) => {
	switch (action.type) {
		case "DELETE_POST": {
			return state.filter(blog => blog.id !== action.payload);
		}
		case "EDIT_POST": {
			return state.map(post => {
				return post.id === action.payload.id ? action.payload : post;
			});
		}
		case "GET_BLOGPOSTS": {
			return action.payload;
		}
		default:
			return state;
	}
};

const getBlogPosts = dispatch => {
	return async () => {
		const response = await jsonServer.get("/blogposts");
		dispatch({ type: "GET_BLOGPOSTS", payload: response.data });
	};
};

const addBlogPost = dispatch => {
	return async (title, content, callback) => {
		await jsonServer.post("/blogposts", { title, content });
		if (callback) {
			callback();
		}
	};
};

const deleteBlogPost = dispatch => {
	return async id => {
		await jsonServer.delete(`/blogposts/${id}`);
		dispatch({
			type: "DELETE_POST",
			payload: id
		});
	};
};

const editBlogPost = dispatch => {
	return async (id, title, content, callback) => {
		jsonServer.put(`/blogposts/${id}`, { title, content });
		dispatch({
			type: "EDIT_POST",
			payload: { id, title, content }
		});
		if (callback) {
			callback();
		}
	};
};

export const { Context, Provider } = createDataContext(
	blogReducer,
	{ addBlogPost, deleteBlogPost, editBlogPost, getBlogPosts },
	[]
);
